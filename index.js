const express = require('express');
const app = express();
const { PORT = 3000 } = process.env;
const path = require('path');

app.use(express.json());
app.use(express.urlencoded({ extended: false }))

app.use('/assets', express.static( path.join( __dirname, 'public', 'static')));

app.get('/', (req, res) => {
    return res.sendFile(path.join( __dirname, 'public', 'index.html'));
});

app.get('/home', (req, res) => {
    return res.sendFile(path.join( __dirname, 'public', 'index.html'));
});

app.get('/about', (req, res) => {
    return res.sendFile(path.join( __dirname, 'public', 'about.html'));
});

app.get('/menu', (req, res) => {
    return res.sendFile(path.join( __dirname, 'public', 'menu.html'));
});

app.get('/contact', (req, res) => {
    return res.sendFile(path.join( __dirname, 'public', 'contact.html'));
});

app.get('/*', (req, res) => {
    res.send('404 Page not found');
});

app.post('/contact', (req, res) => {
    const {contactForm} = req.body;
    return res.sendFile(path.join( __dirname, 'public', 'success.html'));
});

app.listen(PORT, ()=> console.log(`Server started on port ${PORT}...`));